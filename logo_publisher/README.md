1. build the repo as a node (catkin build ros_logo_publisher)
2. run rosrun `logo_publisher logo_publisher logo.jpg` from the .. directory.
3. open rviz, add an Image, and subscribe to the `camera/logo` topic.
4. move the new Image view to wherever you want it displayed